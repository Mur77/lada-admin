import { Admin } from 'react-admin'
import jsonServerProvider from 'ra-data-json-server'

import './App.css'

const dataProvider = jsonServerProvider('https://jsonplaceholder.typicode.com')

function App() {
    return (
        <Admin dataProvider={dataProvider} />
    )
}

export default App
